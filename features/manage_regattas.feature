Feature: Manage regattas
  In order to use be able to have a regatta
  users
  wants to create regatta's

  Scenario: Register new regatta
    Given I am on the regatta's new page
    And I fill in "regatta_name" with "My Regatta"
    When I press "Create"
    Then a regatta should exist with name: "My Regatta"